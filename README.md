# Genome Annotation Benchmarking!

[![Nextflow](https://img.shields.io/badge/nextflow%20DSL2-%E2%89%A522.10.1-23aa62.svg)](https://www.nextflow.io/)
[![run with singularity](https://img.shields.io/badge/run%20with-singularity-1d355c.svg?labelColor=000000)](https://sylabs.io/docs/)

Genome Annotation Tools
>- Braker3
>- Helixer

Required Dependencies:
>- Nextflow
>- Singularity
>- .gm_key in home directory: http://exon.gatech.edu/genemark/license_download.cgi

#### Provide desired inputs in a yaml file
   ```
   module load nextflow
   module load singularity

   SINGULARITY_TMPDIR=/core/labs/Wegrzyn/easel/tmp
   export SINGULARITY_TMPDIR

   nextflow run -hub gitlab PlantGenomicsLab/benchmarking-nf -profile singularity,xanadu \
       -params-file params.yaml
   ```
**params.yaml:**
```
genome : "/core/labs/Wegrzyn/easel/comp_genomics/data/arabidopsis.fasta"
bam : "/core/labs/Wegrzyn/easel/benchmarking/easel/v_1.3/plant/arabidopsis/arabidopsis/03_alignments/bam/*.bam"
busco_lineage : "embryophyta"
prefix : "arabidopsis"
refseq_db : "/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd"
orthodb_db : "/core/labs/Wegrzyn/easel/easel-nf/easel-benchmarking-nf/arabidopsis/work/21/b20ac77350081f3bd82401e15bf2a3/no_3702_Brassicales_orthodb.fasta"
reference : "/core/labs/Wegrzyn/easel/comp_genomics/data/arabidopsis.gtf"
helixer_lineage : "land_plant"
singularity_cache_dir: "/isg/shared/databases/nfx_singularity_cache"
```
**Notes:**
>- Copy and paste a relevant `busco_lineage` from the following [`file`](https://gitlab.com/PlantGenomicsLab/easel/-/blob/main/bin/busco_lineage.txt), capital letters matter!
>- `orthodb_db` is the path to the orthodb database without species (EASEL will generate this, but you need to look in work directory) 
>- `refseq_db` will be either complete or plant refseq DIAMOND database
>- Helixer provides lineage options (--helixer_lineage): `land_plant`, `vertebrate`, `invertebrate`, and `fungi`
>- Make sure you provide a soft masked genome
>- REFERENCE GTF may cause Mikado to fail, I suggest running it quickly through gffread to re-format it:
   `gffread my.gff3 -T -o my.gtf`

Accesions to pick from!
Please put your name next to species you're interested in running in the Google sheet.

[`Google Sheet`](https://docs.google.com/spreadsheets/d/1SwV5kHMBxkN4qqF2iDAyDikHFYKYvx95iakI7KTyqyk/edit#gid=1918712818)

