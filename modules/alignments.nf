process hisat2_index {
	label 'process_medium'

	conda "bioconda::hisat2=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/hisat2:2.2.1--py38he1b5a44_0' :
        'quay.io/biocontainers/hisat2:2.2.1--py38he1b5a44_0' }"

    	input:
    	path(genome)

    	output:
    	path "hisat2.index" , emit: hisat2index
    

    	script:
    	"""
    	hisat2-build -f ${genome} hisat2
		mkdir hisat2.index
		mv *.ht2 hisat2.index
    	"""
}


process hisat2_align {
	publishDir "$params.outdir/alignments/mapping_rates",  mode: 'copy', pattern: "*.txt"
	tag { id }
	label 'process_medium'	

	conda "bioconda::hisat2=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/hisat2:2.2.1--py38he1b5a44_0' :
        'quay.io/biocontainers/hisat2:2.2.1--py38he1b5a44_0' }"

    	input:
    	tuple val(id), path(trimmed)
		path(index)
		val(min)
		val(max)

    	output:
    	tuple val(id), path("*.sam"), emit: sam
		tuple val(id), path("*_mapping_rate.txt"), path("*.sam"), emit: sam_tuple
    

    	script:
    	""" 
        hisat2 -q -x ${index}/hisat2 -1 ${trimmed[0]} -2 ${trimmed[1]} -S ${id}.sam --min-intronlen ${min} --max-intronlen ${max} --summary-file ${id}_mapping_rate.txt
    	"""
}
process removing_samples {
	publishDir "$params.outdir/alignments/bam",  mode: 'copy', pattern: '*.bam'
	tag { id }	
	label 'process_low'

	conda "bioconda::samtools=1.9"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/samtools:1.9--h10a08f8_12' :
        'quay.io/biocontainers/samtools:1.9--h10a08f8_12' }"


    	input:
    	tuple val(id), path(mapping_rate), path(sam)
		val(rate)

    	output:
		tuple val(id), path("*.bam"), optional:true, emit: pass_bam_tuple
		path("*.bam"), optional:true, emit: pass_bam
		path("*mr.txt"), emit: mr
    
    	script:
    	""" 
		rate=\$(awk '/overall alignment rate/ {gsub(/%/,"",\$1); print \$1}' ${mapping_rate})
		rate_var=\$(echo \$rate)

		if [[ "\${rate_var}" < "${rate}" ]]
		then
        echo "${id}'s mapping rate is less than ${rate}% and has been removed" > ${id}_mr.txt
		else
		samtools view -b -@ 16 ${sam} | samtools sort -o sorted_${id}.bam -@ 16
		echo "${id} \$rate_var" > ${id}_mr.txt
		fi 

    	"""
}
process log_mr {
	publishDir "$params.outdir/log",  mode: 'copy'
	label 'process_single'
   
    input:
	path(mr)
	val(rate)

	output:
	path("log_align.txt"), emit: log_align

    script:
    """ 
	header="library rate"
	echo "" > log_align.txt
	echo "##### Alignment Rates #####" >> log_align.txt

	mkdir bam
	mkdir removed
	mkdir kept
	mv ${mr} bam

	for f in bam/*
	do
	if grep -q 'and has been removed' \$f
	then
    mv \$f removed
	else
	mv \$f kept
	fi
	done

	if [ -z "\$(ls -A kept)" ]; then
	echo "ERROR: All libraries have a mapping rate less than ${rate}% and have been removed. Reduce cut off with --rate <int> and -resume."
    exit 1
	else
    cat kept/* > kept.txt
	echo "Passed:" >> log_align.txt
	echo "\$header" >> log_align.txt
	cat kept.txt | while read line; do echo "\$line"; done >> log_align.txt
	fi

	if [ -z "\$(ls -A removed)" ]; then
	echo "" >> log_align.txt
	echo "Failed:" >> log_align.txt
    echo "None." >> log_align.txt
	echo "All libraries have a mapping rate greater than ${rate}%" >> log_align.txt
	else
    cat removed/* > removed.txt
	echo "" >> log_align.txt
	echo "Failed:" >> log_align.txt
	cat removed.txt | while read line; do echo "\$line"; done >> log_align.txt
	fi
    """
}
