process merge_bam {
    label 'process_medium'
    conda "bioconda::samtools=1.16.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/samtools:1.16.1--h00cdaf9_2' :
        'quay.io/biocontainers/samtools:1.16.1--h00cdaf9_2' }"

    input:
    path(bam)
    val(id)
     
    output: 
    path("*_merged.bam"), emit: samtools
      
    """
    samtools merge ${id}_merged.bam ${bam} -@ ${task.cpus}
    """   
}


process braker3 {
    publishDir "$params.outdir/braker3",  mode: 'copy'
    container '/core/labs/Wegrzyn/easel/comp_genomics/braker3/braker3.sif'
    label 'process_high_cpu'

    input:
    path(genome)
    path(protein)
    val(prefix)
    path(bam)
     
    output: 
    path("${prefix}/*"), emit: prediction
    path("${prefix}/braker.gtf"), emit: gtf
      
    """
    braker.pl --genome=${genome} --prot_seq=${protein} --bam=${bam} --threads=${task.cpus} 
    mv braker ${prefix}
    """   
}
