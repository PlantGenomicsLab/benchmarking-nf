process TRINITY_GENOMEGUIDED {
    tag "$meta.id"
    label 'process_high'
    
    conda (params.enable_conda ? "bioconda::trinity=2.13.2" : null)
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/trinity:2.13.2--h00214ad_1':
        'quay.io/biocontainers/trinity:2.13.2--h00214ad_1' }"

    input:
    tuple val(meta), path(bam)
    val(max_intron_size)

    output:
    tuple val(meta),path("transcriptome_trinity/Trinity-GG.fasta"), emit: fasta
    path "versions.yml"           , emit: versions

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    trinity_option = ( meta.strandedness == "unstranded" ) ? "" : "--SS_lib_type RF"
    """
    Trinity --genome_guided_bam $bam \
       --genome_guided_max_intron ${max_intron_size} \
       --CPU ${task.cpus} \
       --max_memory ${task.memory.toGiga()-1}G \
       --output transcriptome_trinity \
       $trinity_option
   
    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        trinity: \$(echo \$(Trinity --version ) | grep "Trinity version" | cut -f3 -d" " | sed "s/Trinity-//" )
    END_VERSIONS
    """
}

process PASA_ALIGNASSEMBLE {
    tag "$meta.id"
    label 'process_high'
    
    if (params.enable_conda) {
        exit 1, "Conda environments cannot be used when using this version of PASA. Please use docker or singularity containers."
    }
    container 'pasapipeline/pasapipeline:2.5.2'

    input:
    tuple val(meta), path(genome)
    tuple val(meta_t), path(transcripts), path(transcripts_clean),path(transcripts_cln)
    path(pasa_config)
    val(max_intron_size)

    output:
    tuple val(meta), path(pasa_fa_clean),path(pasa_gff_clean),  emit: pasa_out
    tuple val(meta), path(db_name), emit: db

    path "versions.yml"           , emit: versions

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"

    pasa_assemblies_fasta = "pasa_DB_${prefix}.sqlite.assemblies.fasta"
    pasa_assemblies_gff = "pasa_DB_${prefix}.sqlite.pasa_assemblies.gff3"

    pasa_fa_clean = meta.id + ".pasa.fasta"
    pasa_gff_clean = meta.id + "pasa.gff3"

    db_name = "pasa_DB_" + prefix + ".sqlite"

    """
    make_pasa_config.pl --infile ${pasa_config} --trunk $prefix --outfile pasa_DB.config

    \$PASAHOME/Launch_PASA_pipeline.pl \
       --ALIGNERS blat,gmap \
       -c pasa_DB.config -C -R \
       -t $transcripts_clean \
       -T \
       -u $transcripts \
       -I $max_intron_size \
       --transcribed_is_aligned_orient \
       -g $genome \
       --CPU ${task.cpus} \

    cp $pasa_assemblies_fasta $pasa_fa_clean
    cp $pasa_assemblies_gff $pasa_gff_clean

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        pasa: 2.5.2
    END_VERSIONS
    """
}
process PASA_ASMBLSTOTRAINING {
    tag "$meta.id"
    label 'process_medium'
    
    if (params.enable_conda) {
        exit 1, "Conda environments cannot be used when using this version of PASA. Please use docker or singularity containers."
    }
    container 'pasapipeline/pasapipeline:2.5.2'

    input:
    tuple val(meta), path(fasta),path(gff)

    output:
    tuple val(meta), path('*.genome.gff3'), emit: gff
    tuple val(meta), path('*.transdecoder.pep'), emit: fasta
    path "versions.yml"           , emit: versions

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    """
    \$PASAHOME/scripts/pasa_asmbls_to_training_set.dbi \
       --pasa_transcripts_fasta $fasta \
       --pasa_transcripts_gff3 $gff 

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        pasa: 2.5.2
    END_VERSIONS
    """
}
process PASA_SEQCLEAN {
    tag "$meta.id"
    label 'process_medium'
    
    if (params.enable_conda) {
        exit 1, "Conda environments cannot be used when using this version of PASA. Please use docker or singularity containers."
    }
    container 'pasapipeline/pasapipeline:2.5.2'

    input:
    tuple val(meta), path(fasta)

    output:
    tuple val(meta),path(fasta),path("*.clean"),path("*.cln"), emit: fasta
    path "versions.yml"           , emit: versions

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    // Seqclean won't work inside container if the username is not exported to USER
    """
    export USER=${workflow.userName}
    seqclean $fasta -c ${task.cpus}

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        pasa: 2.4.1
    END_VERSIONS
    """
}
process EVIDENCEMODELER_EXECUTE {
    tag "$meta.id"
    label 'process_high'
    
    conda (params.enable_conda ? "bioconda::evidencemodeler=1.1.1" : null)
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/evidencemodeler:1.1.1--hdfd78af_3':
        'quay.io/biocontainers/evidencemodeler:1.1.1--hdfd78af_3' }"

    input:
    tuple val(meta), path(partition)

    output:
    tuple val(meta), path(log_file), emit: log
    path "versions.yml"           , emit: versions

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    log_file = partition.getBaseName() + ".log"
    """
    /usr/local/opt/evidencemodeler-1.1.1/EvmUtils/execute_EVM_commands.pl $partition | tee $log_file

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        evidencemodeler: 1.1.0
    END_VERSIONS
    """
}
process EVIDENCEMODELER_MERGE {
    tag "$meta.id"
    label 'process_medium'
    
    conda (params.enable_conda ? "bioconda::evidencemodeler=1.1.1" : null)
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/evidencemodeler:1.1.1--hdfd78af_3':
        'quay.io/biocontainers/evidencemodeler:1.1.1--hdfd78af_3' }"

    input:
    tuple val(meta), path(partitions)
    tuple val(meta_e),path(logs)
    tuple val(meta_g),path(genome)

    output:
    tuple val(meta), path(partitions), emit: partitions
    path "versions.yml"           , emit: versions

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    evm_out = "evm.out"
    """
    /usr/local/opt/evidencemodeler-1.1.1/EvmUtils/recombine_EVM_partial_outputs.pl --partitions $partitions --output_file_name evm.out
    /usr/local/opt/evidencemodeler-1.1.1/EvmUtils/convert_EVM_outputs_to_GFF3.pl  --partitions $partitions --output $evm_out --genome $genome
    touch done.txt

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        evidencemodeler: 1.1.0
    END_VERSIONS
    """
}
process EVIDENCEMODELER_PARTITION {
    //tag "$meta.id"
    label 'process_medium'
    
    conda (params.enable_conda ? "bioconda::evidencemodeler=1.1.1" : null)
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/evidencemodeler:1.1.1--hdfd78af_3':
        'quay.io/biocontainers/evidencemodeler:1.1.1--hdfd78af_3' }"

    input:
    tuple val(meta), path(genome)
    path(genes)
    path(proteins)
    path(transcripts)
    path(weights)

    output:
    tuple val(meta), path(partitions), emit: partitions
    tuple val(meta), path(evm_commands), emit: commands

    path "versions.yml"           , emit: versions

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    partitions = "partitions_list.out"
    evm_commands = "commands.evm.list"
    protein_options = ""
    transcript_options = ""
    if (proteins) {
       protein_options = "--protein_alignments $proteins"   
    }
    if (transcripts) {
       transcript_options = "--transcript_alignments $transcripts"
    }
    """
    /usr/local/opt/evidencemodeler-1.1.1/EvmUtils/partition_EVM_inputs.pl --genome $genome \
       --gene_predictions $genes \
       --segmentSize 2000000 --overlapSize 200000 --partition_listing $partitions \
       $protein_options $transcript_options

    /usr/local/opt/evidencemodeler-1.1.1/EvmUtils/write_EVM_commands.pl --genome $genome \
       --weights \$PWD/${weights} \
       --gene_predictions $genes \
       --output_file_name evm.out \
       --partitions $partitions > $evm_commands

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        evidencemodeler: 1.1.0
    END_VERSIONS
    """
}
