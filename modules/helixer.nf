process helixer {
    label 'process_high'
    
    publishDir "$params.outdir/helixer",  mode: 'copy'
    container 'gglyptodon/helixer-docker:helixer_v0.3.2_cuda_11.8.0-cudnn8'

    input:
    path(genome)
    val(prefix)
    val(lineage)
     
    output: 
    path("*.gff3"), emit: helixer_ann
      
    """
    fetch_helixer_models.py --lineage ${lineage}
    Helixer.py --fasta-path ${genome} --lineage ${lineage} --gff-output-path ${prefix}.gff3
    """   
}
