process gtf_stringtie2 {
    label 'process_low'
    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1 ' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1 ' }"

    input:
    path(prediction)
    val(prefix)
     
    output: 
    path("*.gtf"), emit: gtf
      
    """
    gffread -E ${prediction} -T -o ${prefix}_stringtie2.gtf
    """   
}
process gtf_braker3 {
   label 'process_low'
    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1 ' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1 ' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.gtf"), emit: gtf

    """
    gffread -E ${prediction} -T -o ${prefix}_braker3.gtf
    """
}
process gtf_helixer {
   label 'process_low'
    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1 ' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1 ' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.gtf"), emit: gtf

    """
    gffread -E ${prediction} -T -o ${prefix}_helixer.gtf
    """
}

process mikado_stringtie2 {
   label 'process_medium'
    publishDir "$params.outdir/stringtie2/mikado",  mode: 'copy'
    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    path(prediction)
    path(reference)
    val(id)
     
    output: 
    path("$id*"), emit: prediction
      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}_stringtie2
    """
}
process mikado_braker3 {
 label 'process_medium'
    publishDir "$params.outdir/braker3/mikado",  mode: 'copy'
    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    path(prediction)
    path(reference)
    val(id)
     
    output: 
    path("$id*"), emit: prediction
      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}_braker3
    """
}
process mikado_helixer {
 label 'process_medium'
    publishDir "$params.outdir/helixer/mikado",  mode: 'copy'
    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    path(prediction)
    path(reference)
    val(id)
     
    output: 
    path("$id*"), emit: prediction
      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}_helixer
    """
}
process protein_stringtie2 {
 label 'process_medium'
    input:
    path(genome)
    path(unfiltered_prediction)
    val(id)

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
     
    output: 
    path("*.pep"), emit: protein

    """
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} --protein -o "$id"_stringtie2.pep 
    """
}
process protein_braker3 {
 label 'process_medium'
    input:
    path(genome)
    path(unfiltered_prediction)
    val(id)

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
     
    output: 
    path("*.pep"), emit: protein

    """
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} --protein -o "$id"_braker3.pep 
    """
}
process protein_helixer {
 label 'process_medium'
    input:
    path(genome)
    path(unfiltered_prediction)
    val(id)

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
     
    output: 
    path("*.pep"), emit: protein

    """
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} --protein -o "$id"_helixer.pep 
    """
}
process busco_stringtie2 {
 label 'process_medium'
    publishDir "$params.outdir/stringtie2/busco",  mode: 'copy' 

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.6--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.6--pyhdfd78af_0' }"

    input:
    path(protein)
    val(odb)
    val(prefix)

    output:
    path("$prefix*/*"), emit: busco_unfiltered
    path("$prefix*/*.txt"), emit: busco_unfiltered_txt
      
    """
    busco -i ${protein} -l ${odb} -o ${prefix}_stringtie2 -m Protein

    """
}
process busco_braker3 {
 label 'process_medium'
    publishDir "$params.outdir/braker3/busco",  mode: 'copy' 

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.6--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.6--pyhdfd78af_0' }"

    input:
    path(protein)
    val(odb)
    val(prefix)

    output:
    path("$prefix*/*"), emit: busco_unfiltered
    path("$prefix*/*.txt"), emit: busco_unfiltered_txt
      
    """
    busco -i ${protein} -l ${odb} -o ${prefix}_braker3 -m Protein

    """
}
process busco_helixer {
 label 'process_medium'
    publishDir "$params.outdir/helixer/busco",  mode: 'copy' 

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.6--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.6--pyhdfd78af_0' }"

    input:
    path(protein)
    val(odb)
    val(prefix)

    output:
    path("$prefix*/*"), emit: busco_unfiltered
    path("$prefix*/*.txt"), emit: busco_unfiltered_txt
      
    """
    busco -i ${protein} -l ${odb} -o ${prefix}_helixer -m Protein

    """
}
process agat_stringtie2 {
 label 'process_medium'
    publishDir "$params.outdir/stringtie2/agat",  mode: 'copy' 

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.txt"), emit: unfiltered_stats
      
    """
    agat_sp_statistics.pl --gff ${prediction} -o ${prefix}_stringtie2.txt
    """
}
process agat_braker3 {
 label 'process_medium'
    publishDir "$params.outdir/braker3/agat",  mode: 'copy' 

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.txt"), emit: unfiltered_stats
      
    """
    agat_sp_statistics.pl --gff ${prediction} -o ${prefix}_braker3.txt
    """
}
process agat_helixer {
 label 'process_medium'
    publishDir "$params.outdir/helixer/agat",  mode: 'copy' 

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.txt"), emit: unfiltered_stats
      
    """
    agat_sp_statistics.pl --gff ${prediction} -o ${prefix}_helixer.txt
    """
}
process diamond_stringtie2 {
 label 'process_medium'
    publishDir "$params.outdir/stringtie2/diamond",  mode: 'copy' 
    
    conda "bioconda::diamond"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/diamond:2.0.6--h56fc30b_0' :
        'quay.io/biocontainers/diamond:2.0.6--h56fc30b_0' }"
   
    input:
    path(protein)
    path(database)
    val(id)
 
    output:
    path("*final.txt"), emit: stats
    path("${id}_stringtie2.txt"), emit: annot
      
    """
   diamond blastp --mid-sensitive -d ${database} -q ${protein} -k 1 -o ${id}_stringtie2.txt
    count1=\$(wc -l < ${id}_stringtie2.txt)
    echo \$count1 > ${id}_stringtie2_final.txt
    """
}
process diamond_braker3 {
 label 'process_medium'
    publishDir "$params.outdir/braker3/diamond",  mode: 'copy' 


    conda "bioconda::diamond"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/diamond:2.0.6--h56fc30b_0' :
        'quay.io/biocontainers/diamond:2.0.6--h56fc30b_0' }"
   
    input:
    path(protein)
    path(database)
    val(id)
 
    output:
    path("*final.txt"), emit: stats
    path("${id}_braker3.txt"), emit: annot
      
    """
   diamond blastp --mid-sensitive -d ${database} -q ${protein} -k 1 -o ${id}_braker3.txt
    count1=\$(wc -l < ${id}_braker3.txt)
    echo \$count1 > ${id}_braker3_final.txt
    """
}
process diamond_helixer {
 label 'process_medium'
    publishDir "$params.outdir/helixer/diamond",  mode: 'copy' 

    conda "bioconda::diamond"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/diamond:2.0.6--h56fc30b_0' :
        'quay.io/biocontainers/diamond:2.0.6--h56fc30b_0' }"
   
    input:
    path(protein)
    path(database)
    val(id)
 
    output:
    path("*final.txt"), emit: stats
    path("${id}_helixer.txt"), emit: annot
      
    """
   diamond blastp --more-sensitive --query-cover 70 --subject-cover 70 -d ${database} -q ${protein} -k 1 -o ${id}_helixer.txt --threads ${task.cpus}
    count1=\$(wc -l < ${id}_helixer.txt)
    echo \$count1 > ${id}_helixer_final.txt
    """
}
