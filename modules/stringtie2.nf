process stringtie2 {
        label 'process_medium' 
        tag { id }

    conda "bioconda::stringtie=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/stringtie:2.2.1--hecb563c_2' :
        'quay.io/biocontainers/stringtie:2.2.1--hecb563c_2' }"

    	input:
        tuple val(id), path(bam)
        val(gap)

    	output:
        path("*.gtf"), emit: stringtie2_gtf 

    	script:
    	""" 
        stringtie ${bam} -p ${task.cpus} -g ${gap} -o ${id}.gtf
    	"""
}

process merge_stringtie2 {
    publishDir "$params.outdir/stringtie2/gtf",  mode: 'copy', pattern: '*gtf'

    label 'process_medium' 
    tag { id }

    conda "bioconda::stringtie=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/stringtie:2.2.1--hecb563c_2' :
        'quay.io/biocontainers/stringtie:2.2.1--hecb563c_2' }"

    	input:
    file(gtf)
	
    	output:
    path("stringtie2.gtf"), emit: merged_gtf

    	script:
    	"""   
	stringtie --merge -p ${task.cpus} -o stringtie2.gtf *.gtf
    	"""
}
process stringtie2_fasta {
    publishDir "$params.outdir/log",  mode: 'copy', pattern: '*txt'

    label 'process_low' 
    tag { id }

    	input:
    path(genome)
    path(gtf)
	
	
    	output:
	path("stringtie_transcripts.fa"), emit: fasta
    path("stringtie_transcripts.gff3"), emit: gff3  
    path("log_stringtie2.txt"), emit: log_stringtie

    	script:
    	"""   
    ${projectDir}/bin/gtf_genome_to_cdna_fasta.pl ${gtf} ${genome} > stringtie_transcripts.fa 
    ${projectDir}/bin/gtf_to_alignment_gff3.pl ${gtf} > stringtie_transcripts.gff3

    transcripts=\$(echo | grep -c ">" stringtie_transcripts.fa)
    echo " " >> log_stringtie2.txt
    echo "##### StringTie2 Transcriptome Asssembly #####" >> log_stringtie2.txt
    echo "\$transcripts transcripts" >> log_stringtie2.txt
    	"""
}
process orf_stringtie2 {

    label 'process_medium'

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.0--pl5321hdfd78af_0' }"

    input:
    path(stringtie2Transcriptome) 
     
    output:
    path "${stringtie2Transcriptome}.transdecoder_dir/longest_orfs.pep", emit:longOrfsPepFiles
    path "${stringtie2Transcriptome}.transdecoder_dir/", emit:LongOrfsDirFiles 
    path "${stringtie2Transcriptome}.transdecoder_dir/__checkpoints_longorfs/", emit:LongOrfsCheckpointsFiles
   //path "*.cmds", emit:longOrfsRootCmds
      
    script:
    """
    TransDecoder.LongOrfs -t ${stringtie2Transcriptome}
    """
}

process eggnog_stringtie2 {
    label 'process_medium' 

    conda "bioconda::eggnog-mapper"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.9--pyhdfd78af_0' :
        'quay.io/biocontainers/eggnog-mapper:2.1.9--pyhdfd78af_0' }"

    input:
    path(stringtiePep)  
    path(db)  

    output:
    path "*.hits", emit:eggnogBlastp 
   
    script:
    """
    emapper.py -i ${stringtiePep} --dmnd_db ${db} --no_annot -o eggnog.blastp -m diamond --cpu ${task.cpus}

    """
}

process predict_stringtie2 {
    label 'process_medium'
    
    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.0--pl5321hdfd78af_0' }"

    input:
    path(stringtie2TranscriptomePredict) 
    path(eggnogBlastp)
    path(directory)
     
    output:
    path "${stringtie2TranscriptomePredict}.transdecoder.pep", emit: transdecoderPEP
    path "${stringtie2TranscriptomePredict}.transdecoder.bed", emit: transdecoderBED
    path "${stringtie2TranscriptomePredict}.transdecoder.cds", emit: transdecoderCDS
    path "${stringtie2TranscriptomePredict}.transdecoder.gff3", emit: transdecoderGFF3

    script:
    """
    TransDecoder.Predict -t ${stringtie2TranscriptomePredict} --no_refine_starts --retain_blastp_hits ${eggnogBlastp}

    """
}
process genemodel_stringtie2 {
    publishDir "$params.outdir/stringtie2",  mode: 'copy'
    label 'process_medium'

    input:
    path(stringtie2TranscriptomePredict) 
    path(gff3)
    path(transcript)
     
    output:
    path("stringtie2.fasta.transdecoder.genome.gff3"), emit: genomeGFF3

    script:
    """
    ${projectDir}/bin/PerlLib/*.pm .
      ${projectDir}/bin/cdna_alignment_orf_to_genome_orf.pl \\
        ${stringtie2TranscriptomePredict} \\
        ${gff3} \\
        ${transcript} > stringtie2.fasta.transdecoder.genome.gff3
    """
}
