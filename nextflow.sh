#!/bin/bash
#SBATCH --job-name=benchmarking
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-user=

module load nextflow
module load singularity


nextflow run -hub gitlab PlantGenomicsLab/benchmarking-nf -profile singularity,xanadu -params-file params.yaml 

