include { percent_masked; fold_genome } from '../modules/split_genome.nf'
include { merge_bam; braker3 } from '../modules/braker3.nf'
include { helixer } from '../modules/helixer.nf'
include { gtf_braker3; gtf_helixer; mikado_braker3; mikado_helixer; protein_braker3; protein_helixer; busco_braker3; busco_helixer; agat_stringtie2; agat_braker3; agat_helixer; diamond_braker3; diamond_helixer } from '../modules/metrics.nf'

workflow data_input {

	main:
    percent_masked      				( params.genome )
    fold_genome         				( params.genome, params.prefix )
    bam = Channel.fromPath(params.bam).flatten().collect()
    merge_bam							( bam, params.prefix)
    braker3								( fold_genome.out.fold, params.orthodb_db, params.prefix, merge_bam.out.samtools)

    helixer								( fold_genome.out.fold, params.prefix, params.helixer_lineage )

    gtf_braker3							( braker3.out.gtf, params.prefix )
    gtf_helixer							( helixer.out.helixer_ann, params.prefix )

    mikado_braker3						( gtf_braker3.out.gtf, params.reference, params.prefix )
    mikado_helixer						( gtf_helixer.out.gtf, params.reference, params.prefix )

    protein_braker3						( fold_genome.out.fold, braker3.out.gtf, params.prefix )
    protein_helixer						( fold_genome.out.fold, helixer.out.helixer_ann, params.prefix )

    busco_braker3						( protein_braker3.out.protein, params.busco_lineage, params.prefix )
    busco_helixer						( protein_helixer.out.protein, params.busco_lineage, params.prefix )

    agat_braker3                     	( braker3.out.gtf, params.prefix )
    agat_helixer                     	( helixer.out.helixer_ann, params.prefix )

    diamond_braker3                  	( protein_braker3.out.protein, params.refseq_db, params.prefix )
    diamond_helixer                  	( protein_helixer.out.protein, params.refseq_db, params.prefix ) 
}
